FROM ubuntu:20.04
LABEL maintainer='František Dvořák <valtri@civ.zcu.cz>'

RUN apt-get update && apt-get install -y --no-install-recommends \
    bind9-host \
    bridge-utils \
    dhcpdump \
    dnsutils \
    iperf \
    iputils-ping \
    net-tools \
    tcpdump \
    telnet \
    traceroute \
 && rm -rf /var/lib/apt/lists/*
